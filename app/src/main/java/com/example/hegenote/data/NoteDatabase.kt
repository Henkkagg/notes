package com.example.hegenote.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.hegenote.model.Note

@Database(
    entities = [Note::class],
    version = 1
)
abstract class NoteDatabase: RoomDatabase(){

    abstract fun getNoteDao(): NoteDao

    companion object {
        @Volatile
        private var instance: NoteDatabase? = null

        fun getDatabase(context: Context): NoteDatabase{
            var localInstance = instance
            if (localInstance == null) {
                synchronized(this){
                    localInstance = Room.databaseBuilder(context, NoteDatabase::class.java,
                            "note_database").build()
                    instance = localInstance
                }
            }
            return localInstance!!
        }
    }
}