package com.example.hegenote.data

import androidx.lifecycle.LiveData
import com.example.hegenote.model.Note

class NoteRepository(private val noteDao: NoteDao) {

    val getAllData: LiveData<List<Note>> = noteDao.getAllNotes()

    suspend fun addNote(note: Note) {
        noteDao.addNote(note)
    }

    suspend fun updateNote(note: Note) {
        noteDao.updateNote(note)
    }

    suspend fun deleteNote(note: Note) {
        noteDao.deleteNote(note)
    }

}