package com.example.hegenote.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.hegenote.R

class ActionbarViewModel(application: Application): AndroidViewModel(application) {

    /*
    This viewmodel is the brain of the action bar. Fragments are observing corresponding
    livedata booleans to know when they should perform actions. ActionbarFragment
    observes icon - variables to know which action buttons to show.
     */

    //Observed by NotesFragment
    private val _addPressed = MutableLiveData<Boolean>()
    val addPressed: LiveData<Boolean> get() = _addPressed

    //Observed by UpdateFragment
    private val _savePressed = MutableLiveData<Boolean>()
    val savePressed: LiveData<Boolean> get() = _savePressed

    //Observed by UpdateFragment
    private val _arrowPressed = MutableLiveData<Boolean>()
    val arrowPressed: LiveData<Boolean> get() = _arrowPressed

    //Observed by UpdateFragment
    private val _deletePressed = MutableLiveData<Boolean>()
    val deletePressed: LiveData<Boolean> get() = _deletePressed

    //Observed by ActionbarFragment. Containts the action bar title
    private val _fragmentbasedTitle = MutableLiveData<String>()
    val fragmentbasedTitle: LiveData<String> get() = _fragmentbasedTitle

    //Observed by ActionbarFragment. Contains the resource id of either 'add' or 'save' - icon
    private val _fragmentbasedactionIcon = MutableLiveData<Int>()
    val fragmentbasedactionIcon: LiveData<Int> get() = _fragmentbasedactionIcon

    //Observed by ActionbarFragment. Action icon (save) will be hidden if edittexts are empty
    private val _showfragmentbasedactionIcon = MutableLiveData<Boolean>()
    val showfragmentbasedactionIcon: LiveData<Boolean> get() = _showfragmentbasedactionIcon

    //Observed by ActionbarFragment. Back-arrow is shown in UpdateFragment only
    private val _showArrowIcon = MutableLiveData<Boolean>()
    val showArrowIcon: LiveData<Boolean> get() = _showArrowIcon

    //Observed by ActionbarFragment. Delete-icon is only shown if user opened existing note
    private val _showDeleteIcon = MutableLiveData<Boolean>()
    val showDeleteIcon: LiveData<Boolean> get() = _showDeleteIcon

    //This tracks which fragment is currently displayed. Application starts in NotesFragment
    private var inNotesFragment = true


    //Called in UpdateFragment's onResume to setup Actionbar's layout for that fragment
    fun loadUpdateActionbar() {
        _addPressed.value = false
        inNotesFragment = false
        _showArrowIcon.value = true
        _fragmentbasedactionIcon.value = R.drawable.ic_save
        _showfragmentbasedactionIcon.value = false
        _fragmentbasedTitle.value = "Editor"
    }

    //Called in NotesFragment's onResume to setup Actionbar's layout for that fragment
    fun loadNotesActionbar() {
        _savePressed.value = false
        inNotesFragment = true
        _showArrowIcon.value = false
        _fragmentbasedactionIcon.value = R.drawable.ic_add
        _showfragmentbasedactionIcon.value = true
        _fragmentbasedTitle.value = "Notes"
        _showDeleteIcon.value = false
    }

    //Called by ActionbarFragment when action-button in top right corner is pressed.
    fun checkWhatWasPressed() {
        if (inNotesFragment) _addPressed.value = true
        else if (showfragmentbasedactionIcon.value == true) _savePressed.value = true
    }

    //Called by ActionbarFragment when arrow-button is shown AND pressed.
    fun arrowPressed() {
        if (showArrowIcon.value == true)  _arrowPressed.value = true
    }

    //Called by ActionbarFragment when delete-button is shown AND pressed.
    fun deletePressed() {
        if (showDeleteIcon.value == true)_deletePressed.value = true
    }

    //Called by UpdateFragment when keypress has been noted and appropriate action is done
    fun resetArrowPressed() {
        _arrowPressed.value = false
    }

    //Called by UpdateFragment when keypress has been noted and appropriate action is done
    fun resetDeletePressed() {
        _deletePressed.value = false
    }

    //Called by UpdateFragment, which decides whether the save-button should be visible
    fun showfragmentbasedactionIcon(boolean: Boolean) {
        _showfragmentbasedactionIcon.value = boolean
    }

    //Called by UpdateFragment if user entered editor by pressing an existing note
    fun showDeleteIcon() {
        _showDeleteIcon.value = true
    }
}