package com.example.hegenote.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.hegenote.databinding.FragmentActionbarBinding
import com.example.hegenote.viewmodels.ActionbarViewModel
import timber.log.Timber

class ActionbarFragment: Fragment() {

    private lateinit var _binding: FragmentActionbarBinding
    private val binding get() = _binding

    private val actionbarViewModel: ActionbarViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?):
            View? {
        _binding = FragmentActionbarBinding.inflate(inflater, container, false)


        binding.fragmentbasedactionIcon.setOnClickListener {
            actionbarViewModel.checkWhatWasPressed()
        }
        binding.arrowIcon.setOnClickListener {
            actionbarViewModel.arrowPressed()
        }
        binding.deleteIcon.setOnClickListener {
            actionbarViewModel.deletePressed()
        }

        actionbarViewModel.fragmentbasedactionIcon.observe(viewLifecycleOwner, Observer {
            binding.fragmentbasedactionIcon.setImageResource(it)
        })

        actionbarViewModel.showfragmentbasedactionIcon.observe(viewLifecycleOwner, Observer {
            if (it) binding.fragmentbasedactionIcon.alpha = 1.0f
            else binding.fragmentbasedactionIcon.alpha = 0.0f
        })

        actionbarViewModel.showArrowIcon.observe(viewLifecycleOwner, Observer {
            if (it) binding.arrowIcon.alpha = 1.0f
            else binding.arrowIcon.alpha = 0.0f
        })

        actionbarViewModel.showDeleteIcon.observe(viewLifecycleOwner, Observer {
            if (it) binding.deleteIcon.alpha = 1.0f
            else binding.deleteIcon.alpha = 0.0f
        })

        actionbarViewModel.fragmentbasedTitle.observe(viewLifecycleOwner, Observer {
            binding.toolbarTitle.text = it
        })

        return binding.root
    }
}