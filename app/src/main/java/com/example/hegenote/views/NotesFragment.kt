package com.example.hegenote.views

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hegenote.R
import com.example.hegenote.databinding.FragmentNotesBinding
import com.example.hegenote.viewmodels.ActionbarViewModel
import com.example.hegenote.viewmodels.NoteViewModel
import timber.log.Timber

class NotesFragment : Fragment() {
    private var _binding: FragmentNotesBinding? = null
    private val binding get() = _binding!!

    private val noteViewModel: NoteViewModel by activityViewModels()
    private val actionbarViewModel: ActionbarViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNotesBinding.inflate(inflater, container, false)

        //RecyclerView setup
        val adapter = NotesRVAdapter()
        val recyclerView = binding.rvNotes
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        //Writing to database is still in progress when user returns here. Update UI using old data
        //and apply changes to recyclerview according to safe args
        var cachedNoteList = noteViewModel.readAllNotes.value
        val args by navArgs<NotesFragmentArgs>()
        val currentNote = args.currentNote      //Note that was edited in UpdateFragment
        val rvPosition = args.rvPosition        //Index position in recyclerView (starting from 1)
        val deleteNote = args.deleteNote        //Boolean. True if pressed delete in UpdateFragment

        if (cachedNoteList != null) {
            adapter.setData(cachedNoteList)

            if (deleteNote) adapter.deleteNote(rvPosition)
            else {
                if (currentNote!=null && rvPosition==0) adapter.addNote(currentNote)
                if (currentNote!=null && rvPosition!=0) adapter.updateNote(currentNote, rvPosition)
            }
        }

        //After db coroutine has finished, update rv adapter with real list to include Primary Key
        noteViewModel.readAllNotes.observe(viewLifecycleOwner, Observer {
            if (it != cachedNoteList) adapter.setData(it)
        })

        actionbarViewModel.addPressed.observe(viewLifecycleOwner, Observer {
            if (it)  findNavController().navigate(R.id.action_notesFragment_to_updateFragment)
        })

        return binding.root
    }

    override fun onResume() {
        actionbarViewModel.loadNotesActionbar()
        closeKeyboard()
        super.onResume()
    }

    private fun closeKeyboard() {
        val inputMethodManager = activity
            ?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)
    }
}