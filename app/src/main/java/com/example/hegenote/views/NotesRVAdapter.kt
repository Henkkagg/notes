package com.example.hegenote.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.hegenote.model.Note
import com.example.hegenote.databinding.NotervLayoutBinding

class NotesRVAdapter : RecyclerView.Adapter<NotesRVAdapter.ViewHolder>(){


    private var notesList = emptyList<Note>()

    class ViewHolder(val binding: NotervLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(NotervLayoutBinding
                .inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = notesList[position]
        holder.binding.txtrvTitle.text = currentItem.title
        holder.binding.txtrvNote.text = currentItem.note

        holder.binding.notervLayout.setOnClickListener {

            //This check prevents user from selecting a note that hasn't been saved to db yet
            if (currentItem.id != 0) {
                val action = NotesFragmentDirections
                    .actionNotesFragmentToUpdateFragment(currentItem, (position+1))
                holder.binding.root.findNavController().navigate(action)
            }
        }
    }

    override fun getItemCount(): Int {
        return notesList.size
    }

    fun setData(note: List<Note>) {
        notesList = note
        notifyDataSetChanged()
    }

    fun updateNote(currentNote: Note, rvPosition: Int) {
        val listPosition = rvPosition - 1
        val temporaryList = notesList.toMutableList()
        temporaryList[listPosition] = currentNote
        notesList = temporaryList.toList()
        notifyDataSetChanged()
    }

    fun addNote(currentNote: Note) {
        val temporaryList = notesList.toMutableList()
        temporaryList.add(currentNote)
        notesList = temporaryList.toList()
        notifyDataSetChanged()
    }

    fun deleteNote(rvPosition: Int) {
        val listPosition = rvPosition - 1
        val temporaryList = notesList.toMutableList()
        temporaryList.removeAt(listPosition)
        notesList = temporaryList.toList()
        notifyDataSetChanged()
    }
}