package com.example.hegenote.views

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.hegenote.model.Note
import com.example.hegenote.databinding.FragmentUpdateBinding
import com.example.hegenote.viewmodels.ActionbarViewModel
import com.example.hegenote.viewmodels.NoteViewModel
import timber.log.Timber

class UpdateFragment: Fragment() {
    private var _binding: FragmentUpdateBinding? = null
    private val binding get() = _binding!!

    private val noteViewModel: NoteViewModel by activityViewModels()
    private val actionbarViewModel: ActionbarViewModel by activityViewModels()

    private var txtTitle = ""
    private var txtNote = ""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUpdateBinding.inflate(layoutInflater, container, false)

        //If user navigated here by pressing a specific note, get args and enable delete-button
        val args by navArgs<UpdateFragmentArgs>()
        var id = 0
        val rvPosition = args.rvPosition
        if (args.currentNote!=null) {
            binding.etxtTitle.setText(args.currentNote!!.title)
            binding.etxtNote.setText(args.currentNote!!.note)
            id = args.currentNote!!.id
            actionbarViewModel.showDeleteIcon()
        }

        //Listeners call a function to decide if save-icon should be shown
        binding.etxtTitle.addTextChangedListener { observeIfFieldsHaveText() }
        binding.etxtNote.addTextChangedListener {  observeIfFieldsHaveText() }

        val discardDialog = AlertDialog.Builder(context)
                .setTitle("Discard changes?")
                .setPositiveButton("Yes") { _, _ -> exitWithoutSaving() }
                .setNegativeButton("No") { _, _ -> actionbarViewModel.resetArrowPressed() }
                .create()

        val deleteDialog = AlertDialog.Builder(context)
                .setTitle("Delete note?")
                .setPositiveButton("Yes") { _, _ -> deleteNote(id, rvPosition) }
                .setNegativeButton("No") { _, _ -> actionbarViewModel.resetDeletePressed() }
                .create()

        //Observers for Actionbar - keypresses
        actionbarViewModel.savePressed.observe(viewLifecycleOwner, Observer {
            if (it) saveNote(id, rvPosition)
        })

        actionbarViewModel.arrowPressed.observe(viewLifecycleOwner, Observer {
            if (it) ifUnsavedAskIfSure(discardDialog)
        })

        actionbarViewModel.deletePressed.observe(viewLifecycleOwner, Observer {
            if (it) {
                deleteDialog.show()
                actionbarViewModel.resetDeletePressed()
            }
        })


        //Same functionality as activities' onBackPressed()
        val backPressed = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                ifUnsavedAskIfSure(discardDialog)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, backPressed)

        return binding.root
    }

    //Called by edittext listeners to hide save-button if either of the edittexts are empty
    private fun observeIfFieldsHaveText() {
        txtTitle = binding.etxtTitle.text.toString().trim()
        txtNote = binding.etxtNote.text.toString().trim()

        if (txtTitle.isEmpty() || txtNote.isEmpty()) {
            actionbarViewModel.showfragmentbasedactionIcon(false)
        } else actionbarViewModel.showfragmentbasedactionIcon(true)
    }

    //Called when user presses back on action bar or system ui and has unsaved information
    private fun ifUnsavedAskIfSure(discardDialog: AlertDialog) {
        if (txtTitle.isEmpty() && txtNote.isEmpty()) {
            exitWithoutSaving()
        } else { discardDialog.show()

        }
    }

    private fun exitWithoutSaving() {
        actionbarViewModel.resetArrowPressed()
        findNavController().navigate(UpdateFragmentDirections.actionUpdateFragmentToNotesFragment())
    }


    private fun saveNote(id: Int, rvPosition: Int) {
        val note = Note(id, txtTitle, txtNote)

        //ID == 0 if fragment wasn't supplied with args, and therefore new entry is created
        if (id == 0) noteViewModel.addNote(note)
        else noteViewModel.updateNote(note)

        val action = UpdateFragmentDirections
            .actionUpdateFragmentToNotesFragment(note, rvPosition)
        findNavController().navigate(action)
    }

    private fun deleteNote(id: Int, rvPosition: Int) {
        val note = Note(id, txtTitle, txtNote)

        noteViewModel.deleteNote(note)

        val action = UpdateFragmentDirections
            .actionUpdateFragmentToNotesFragment(note, rvPosition, true)
        findNavController().navigate(action)
    }

    override fun onResume() {
        actionbarViewModel.loadUpdateActionbar()
        super.onResume()
    }




}